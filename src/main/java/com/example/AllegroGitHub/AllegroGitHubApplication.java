package com.example.AllegroGitHub;

import com.example.AllegroGitHub.events.GitRestContent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AllegroGitHubApplication {

    public static void main(String[] args) {

        SpringApplication.run(AllegroGitHubApplication.class, args);

    }

}
