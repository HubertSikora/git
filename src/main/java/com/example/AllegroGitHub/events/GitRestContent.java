package com.example.AllegroGitHub.events;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * GitRestContent mapuje wartości JSONA z githuba do obiektu w javie.
 *
 * @URL - Adres URL sortujący repozytoria na profilu Allegro.
 * Dokumentacja API GitHub https://developer.github.com/v3/search/legacy/
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GitRestContent {

    final String URL = "https://api.github.com/users/allegro/repos?sort=updated&direction=desc";

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "updated_at")
    private Date updatedAt;

}
