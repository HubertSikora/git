package com.example.AllegroGitHub.events;


import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class GithubService {

    private final RestTemplate restTemplate;

    public GithubService(RestTemplateBuilder restTemplateBuilder) {

        restTemplate = restTemplateBuilder.build();
    }

    public List<GitRestContent> getFromApi(String URL) {
        RestTemplate restTemplate = new RestTemplate();
        GitRestContent[] res = restTemplate.getForObject(URL, GitRestContent[].class);
        List<GitRestContent> restContents = Arrays.asList(res);
        return (restContents.subList(0, 1));
    }


}
