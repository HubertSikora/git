package com.example.AllegroGitHub.events;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class GithubController {

    GitRestContent URL = new GitRestContent();

    private final GithubService githubService;

    public GithubController(GithubService githubService) {
        this.githubService = githubService;
    }

/*
    //@GetMapping obsługują metodę HTTP PATCH. Nowsza wersja RequestMapping
    @GetMapping("/index") //podstrona
    public String homepage(Model model) {
        GitRestContent gitRestContent = githubService.getGitRestContent(URL.URL);
        model.addAttribute("repository", gitRestContent); // co wyswietlic

        return "index"; //nazwa pliku
    }
*/

    @GetMapping("/")

    public ResponseEntity<List<GitRestContent>> getFromApi() {

        List<GitRestContent> restContents = githubService.getFromApi(URL.URL);

        return ResponseEntity.ok(restContents.subList(0, 1));

    }
}

