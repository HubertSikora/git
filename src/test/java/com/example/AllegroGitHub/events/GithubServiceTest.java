package com.example.AllegroGitHub.events;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Unit test Githubservice
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GithubServiceTest {
    final String URL = "https://api.github.com/users/allegro/repos?sort=updated&direction=desc";

    @Mock
    private RestTemplateBuilder restTemplateBuilder;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private GithubService githubService;

    @Test
    public void commitTryTest() { //sprawdzamy ilosc komitów
        when(restTemplateBuilder.build()).thenReturn(restTemplate);


        GitRestContent[] gitRestContent = new GitRestContent[1];
        //given
        when(restTemplate.getForObject(URL, GitRestContent[].class

        )).thenReturn(gitRestContent);

        //when
        List<GitRestContent> underTest = githubService
                .getFromApi(URL);
        //then
        Assertions.assertThat(underTest.size()).isEqualTo(gitRestContent.length);
    }

}
